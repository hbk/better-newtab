const MAX_SITES = 6;
const MAX_CATEGORIES = 16;

const initialContent = {
    categories: [],
    sites: []
};

let content = initialContent;

const toggleDisplay = element => {
    if (element.style.display === "none") {
        return "block";
    } else {
        return "none";
    }
};


const init = () => {
    load();

    const categoryButton = document.querySelector("#category-button");
    const siteButton = document.querySelector("#site-button");
    const settingsButton = document.querySelector("#settings-button");
    const categoryForm = document.querySelector("#category-form");
    const siteForm = document.querySelector("#site-form");
    const settingsForm = document.querySelector("#settings-form");
    const exportButton = document.querySelector("#export-button");
    const importButton = document.querySelector("#import-button");
    const resetButton = document.querySelector("#reset-button");
    const undoButton = document.querySelector("#undo-button");
    categoryForm.style.display = "none";
    siteForm.style.display = "none";
    settingsForm.style.display = "none";

    categoryButton.addEventListener("click", e => {
        siteForm.style.display = "none";
        settingsForm.style.display = "none";
        categoryForm.style.display = toggleDisplay(categoryForm);
    });

    siteButton.addEventListener("click", e => {
        categoryForm.style.display = "none";
        settingsForm.style.display = "none";
        siteForm.style.display = toggleDisplay(siteForm);
    });

    settingsButton.addEventListener("click", e => {
        siteForm.style.display = "none";
        categoryForm.style.display = "none";
        settingsForm.style.display = toggleDisplay(settingsForm);
    });

    categoryForm.addEventListener("submit", e => {
        e.preventDefault();
        const category = e.target[0].value;
        if (category === "") {
            showInfo("error", "You must provide a category name.");
        } else if (content.categories.length === MAX_CATEGORIES) {
            showInfo("error", `You cannot create more than ${MAX_CATEGORIES} categories`);
        } else {
            createCategory(category);
            e.target[0].value = "";
        }
    });

    siteForm.addEventListener("submit", e => {
        e.preventDefault();
        const category = e.target[0].value;
        const site = e.target[1].value;
        const url = e.target[2].value;
        const siteCount = content.sites.filter(site => site.category === category).length;
        if (category === "" || site === "" || url === "") {
            showInfo("error", "You must fill all fields.");
        } else if (siteCount == MAX_SITES) {
            showInfo("error", `You cannot create more than ${MAX_SITES} sites`);
        } else {
            createSite(category, site, url);
            e.target[1].value = "";
            e.target[2].value = "";
        }
    });

    exportButton.addEventListener("click", e => {
        const contentJson = JSON.stringify(content);
        document.write(contentJson);
    });

    importButton.addEventListener("click", e => {
        const importJson = document.querySelector("#import-json");
        if (importJson.value === "") {
            return;
        }
        content = JSON.parse(importJson.value);
        importJson.value = "";
        save();
        load();
    });

    resetButton.addEventListener("click", e => {
        document.querySelector("#sites").innerHTML = "";
        const backup = JSON.stringify(content);
        const initial = JSON.stringify(initialContent);
        localStorage.setItem("content", initial);
        localStorage.setItem("content-backup", backup);
        content = initialContent;
    });

    undoButton.addEventListener("click", e => {
        const backup = localStorage.getItem("content-backup");
        localStorage.setItem("content", backup);
        load();
    });
};

const save = () => {
    const contentJson = JSON.stringify(content);
    localStorage.setItem("content", contentJson);
};

const load = () => {
    const contentJson = localStorage.getItem("content");
    const storedContent = JSON.parse(contentJson);

    if (storedContent === null) {
        content = initialContent;
        return;
    } else {
        content = storedContent;
    }

    if (content.categories.length === 0 && content.sites.length === 0) {
        return;
    } 

    content.categories.forEach(category => makeCategoryElement(category));
    content.sites.forEach(site => makeSiteElement(site.category, site.name, site.url));
    appendCategory(content.categories);
};

const appendCategory = categories => {
    const categorySelect = document.querySelector("#category-select");
    categories.forEach(category => {
        const option = document.createElement("option");
        option.value = category;
        option.innerText = category;
        categorySelect.appendChild(option);
    });
}

// Strip url from its parameters
const stripUrl = url => {
    const splitted = url.split(".");
    let domain, extension;

    if (splitted.length == 2) {
        domain = splitted[0];
        extension = splitted[1].split("/")[0];
    } else {
        domain = splitted[1];
        extension = splitted[2].split("/")[0];
    }

    return `${domain}.${extension}`;
};

// Add http:// if missing
const prependUrl = url => {
    if (url.startsWith("https://") || url.startsWith("http://")) {
        return url;
    } else {
        return `http://${url}`;
    }
};

const getFavicon = url => `https://www.google.com/s2/favicons?domain=${stripUrl(url)}`;

const createCategory = name => {
    makeCategoryElement(name);
    content.categories.push(name);
    appendCategory([name]);
    save();
};

const makeCategoryElement = name => {
    const sites = document.querySelector("#sites");
    const entry = document.createElement("div");
    entry.id = name.toLowerCase();
    entry.className = "entry";

    const category = document.createElement("div");
    category.classList.add("category", "center");
    category.innerText = name;

    entry.append(category);
    sites.append(entry);
}

const createSite = (category, name, link) => {
    const url = prependUrl(link);
    makeSiteElement(category, name, url);
    addToContent(category, name, url);
    save();
}

const makeSiteElement = (category, name, url) => {
    const entry = document.querySelector(`#${category.toLowerCase()}`);
    const site = document.createElement("a");
    site.setAttribute("href", url);
    site.classList.add("site", "v-center")
    site.id = name.toLowerCase();

    const icon = document.createElement("div");
    icon.classList.add("icon", "center");

    const label = document.createElement("div");
    label.className = "name";
    label.innerText = name;

    const image = document.createElement("img");
    image.setAttribute("src", getFavicon(url));

    icon.append(image);
    site.append(icon);
    site.append(label);
    entry.append(site);
};

const addToContent = (category, name, url) => {
    const sortedPositions = content.sites
        .filter(site => site.category === category)
        .map(x => x.position)
        .sort((a, b) => b - a)

    const position = sortedPositions.length > 0 
        ? sortedPositions[0] + 1 : 0;
    
    const site = {
        category,
        name,
        url: prependUrl(url),
        icon: getFavicon(url),
        position
    };

    content.sites.push(site);
};

// Add an information bar for.
const showInfo = (type, message) => {
    switch (type) {
        case "error":
            window.alert(message);
            break;
        default:
            window.alert(message);
    }
};

window.onload = init;

